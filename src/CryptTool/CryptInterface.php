<?php

declare(strict_types=1);

namespace AutoAction\Phalcon\CryptTool;

/**
 * Estrutura para criptografia e descriptografia
 *
 * @package AutoAction\Phalcon\CryptTool
 * @date    21/07/2021 10:39
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
interface CryptInterface
{
    public function encryptString(string $data): string;
    public function decryptString(string $data): string;
}