<?php

declare(strict_types=1);

namespace AutoAction\Phalcon\CryptTool;

use AutoAction\Phalcon\CryptTool\Cipher\CipherInterface;
use Exception;
use Phalcon\Crypt;
use Phalcon\Crypt\Mismatch;

/**
 * Classe principal de criptografia
 *
 * @package AutoAction\Phalcon\CryptTool
 * @date    21/07/2021 10:47
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
class CryptTool implements CryptInterface
{
    /**
     * @var CipherInterface
     */
    private $cipher;

    /**
     * @var CryptConfig
     */
    private $config;

    /**
     * @var Crypt
     */
    private $crypt;

    public function __construct(CryptConfig $config, CipherInterface $cipher)
    {
        $this->cipher = $cipher;
        $this->config = $config;
        $this->crypt = new Crypt($this->cipher->getCipher());
    }

    public function getConfig(): CryptConfig
    {
        return $this->config;
    }

    public function encryptString(string $data): string
    {
        $data = trim($data);
        if (strpos($data, $this->config->getPrefix()) === 0) {
            return $data;
        }

        $base64Encode = base64_encode($this->crypt->encrypt($data, $this->config->getKey()));
        return $this->config->getPrefix() . $base64Encode . $this->config->getSuffix();
    }

    /**
     * @throws Mismatch
     * @throws Exception
     */
    public function decryptString(string $data): string
    {
        $data = trim($data);
        $prefix = $this->config->getPrefix();
        $suffix = $this->config->getSuffix();

        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        // contadores
        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        $prefixSize = strlen($prefix);
        $suffixSize = strlen($suffix);
        $inputSize = strlen($data);
        $contentSize = $inputSize - ($suffixSize + $prefixSize);

        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        // testa se é uma string criptografada pela classe
        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        if (strpos($data, $prefix) === false) {
            return $data;
        }

        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        // testa a existência do sufixo
        // - - - - - - - - - - - - - - - - - - - - - - - - - -
        if (strpos($data, $suffix) != ($inputSize - $suffixSize)) {
            throw new Exception('String truncada');
        }

        if (strpos($data, $prefix) === 0) {
            $clearData = substr($data, $prefixSize, $contentSize);
            return $this->crypt->decrypt(base64_decode($clearData), $this->config->getKey());
        }

        throw new Exception('Erro inesperado!');
    }
}