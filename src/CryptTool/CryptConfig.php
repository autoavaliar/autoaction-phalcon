<?php

declare(strict_types=1);

namespace AutoAction\Phalcon\CryptTool;

/**
 * Configuração do sistema de criptografia
 *
 * @package AutoAction\Phalcon\CryptTool
 * @date    21/07/2021 10:44
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
class CryptConfig
{
    private $key;
    private $prefix;
    private $suffix;
    private $separator = '_!!!_';

    public function __construct(string $key, string $prefix)
    {
        $this->key = $key;
        $this->prefix = $prefix;
        $this->suffix = strrev($this->prefix);
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function getPrefix(): string
    {
        return $this->prefix.$this->separator;
    }

    public function getSuffix(): string
    {
        return $this->separator.$this->suffix;
    }
}