<?php

declare(strict_types=1);

namespace AutoAction\Phalcon\CryptTool\Cipher;

/**
 * Cifra padrão
 *
 * @package AutoAction\Phalcon\CryptTool\Cipher
 * @date    21/07/2021 10:58
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
class CipherAes256Cfb implements CipherInterface
{
    public function getCipher(): string
    {
        return 'aes-256-cfb';
    }
}