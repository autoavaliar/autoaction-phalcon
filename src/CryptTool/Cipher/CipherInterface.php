<?php

declare(strict_types=1);

namespace AutoAction\Phalcon\CryptTool\Cipher;

/**
 * Contrato com os cifras a serem criadas
 *
 * @package AutoAction\Phalcon\CryptTool\Cipher
 * @date    21/07/2021 10:56
 *
 * @author  Fernando Petry <fernando.petry@autoavaliar.com.br>
 */
interface CipherInterface
{
    public function getCipher():string;
}