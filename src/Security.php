<?php

namespace AutoAction\Phalcon;

class Security
{
    /**
     * Work Factor
     *
     * @var int
     * @access protected
     */
    protected $_workFactor = 8;
    /**
     * Number of Bytes
     *
     * @var int
     * @access protected
     */
    protected $_numberBytes = 16;

    /**
     * Alphanumerical Filter
     *
     * @param string $value
     * @return string
     */
    private static function filterAlnum($value)
    {
        $filtered = '';
        $value = (string)$value;
        $valueLength = strlen($value);
        $zeroChar = chr(0);
        for ($i = 0; $i < $valueLength; ++$i) {
            if ($value[$i] == $zeroChar) {
                break;
            }
            if (ctype_alnum($value[$i]) === true) {
                $filtered .= $value[$i];
            }
        }
        return $filtered;
    }

    /**
     * Generate a >22-length pseudo random string to be used as salt for passwords
     *
     * @return string
     * @throws \Exception
     */
    public function getSaltBytes()
    {
        if (function_exists('openssl_random_pseudo_bytes') === false) {
            throw new \Exception('Openssl extension must be loaded');
        }
        $safeBytes = '';
        while (strlen($safeBytes) < 22) {
            $randomBytes = openssl_random_pseudo_bytes($this->_numberBytes);
            //@note added check
            if ($randomBytes === false) {
                throw new \Exception('Error while generating random bytes.');
            }
            $base64bytes = base64_encode($randomBytes);
            $safeBytes = self::filterAlnum($base64bytes);
            if (empty($safeBytes) === true) {
                continue;
            }
        }
        return $safeBytes;
    }

    /**
     * Creates a password hash using bcrypt with a pseudo random salt
     *
     * @param string $password
     * @param int|null $workFactor
     * @return string
     * @throws \Exception(
     */
    public function hash($password, $workFactor = null)
    {
        if (is_string($password) === false) {
            throw new \Exception('Invalid parameter type.');
        }
        if (is_null($workFactor) === true) {
            $workFactor = $this->_workFactor;
        } elseif (is_int($workFactor) === false) {
            throw new \Exception('Invalid parameter type.');
        }
        $factor = sprintf('%02s', $workFactor);
        $saltBytes = $this->getSaltBytes();
        $salt = '$2a$'.$factor.'$'.$saltBytes;
        return crypt($password, $salt);
    }
    /**
     * Checks a plain text password and its hash version to check if the password matches
     *
     * @param string $password
     * @param string $passwordHash
     * @param int|null $maxPasswordLength
     * @return boolean|null
     * @throws \Exception(
     */
    public function checkHash($password, $passwordHash, $maxPasswordLength = null)
    {
        /* Type check */
        if (is_string($password) === false ||
            is_string($passwordHash) === false) {
            throw new \Exception('Invalid parameter type.');
        }
        if (is_int($maxPasswordLength) === true) {
            if ($maxPasswordLength > 0 && strlen($password) > $maxPasswordLength) {
                return false;
            }
        } elseif (is_null($maxPasswordLength) === false) {
            throw new \Exception('Invalid parameter type.');
        }
        /* Hash */
        try {
            $hash = crypt($password, $passwordHash);
        } catch (\Exception $e) {
            return null;
        }
        if (is_string($hash) === false) {
            $hash = (string)$hash;
        }
        if (strlen($hash) === strlen($passwordHash)) {
            $n = strlen($hash);
            $check = false;
            for ($i = 0; $i < $n; ++$i) {
                $check |= (ord($hash[$i])) ^ (ord($passwordHash[$i]));
            }
            return (bool)($check === 0 ? true : false);
        }
        return false;
    }
}