<?php

namespace Test\AutoAction\Phalcon;

use AutoAction\Phalcon\Security;
use PHPUnit\Framework\TestCase;

class SecurityTest extends TestCase
{

    public function testCheckHash()
    {
        $passStr = 'test123';
        $passHash = '$2a$08$LInz6TKGTfFsd4yYnEI4xeGlxBr8LQiu4TRfXeFY//8/xOIfZtDKK';

        $pass = (new Security())->checkHash($passStr, $passHash);

        self::assertTrue($pass);

    }
}
