<?php

namespace Test\AutoAction\Phalcon\CryptTool;

use AutoAction\Phalcon\CryptTool\Cipher\CipherAes256Cfb;
use AutoAction\Phalcon\CryptTool\CryptConfig;
use AutoAction\Phalcon\CryptTool\CryptTool;
use Exception;
use Phalcon\Crypt\Mismatch;
use PHPUnit\Framework\TestCase;

/*
-- Criptografando
[] Entrada de uma string, saida deve ser criptografada
[] Entrada de uma string criptografada, a saida deve ser a propria entrada

-- Descriptografando
[] Entrada de uma string criptografada, a saida deve ser o texto descriptografado
[] Entrada de uma string descriptografada a saida deve ser a propria entrada

--
[] verificar o final do sufixo
*/

class CryptToolTest extends TestCase
{
    private $prefix = 'my-prefix_';
    private $key = 'my-key';

    private function getConfig(): CryptConfig
    {
        return new CryptConfig($this->key, $this->prefix);
    }

    private function getCrypt(): CryptTool
    {
        return new CryptTool($this->getConfig(), new CipherAes256Cfb());
    }

    public function testEncrypt()
    {
        $prefix = $this->getConfig()->getPrefix();
        $suffix = $this->getConfig()->getSuffix();

        $expected = $prefix . 'D1dAglwudXp1C5hR0A13fSsrij/jdmsFbVQQsfo1' . $suffix;
        $string = 'fernando-petry';

        // Entrando uma string criptografada, o retorno deve ser a propria string
        $valueNotAction = $this->getCrypt()->encryptString($expected);
        self::assertEquals($expected, $valueNotAction);

        // Entrando uma qualquer a saida deve ser uma string criptografada
        $valueDefault = $this->getCrypt()->encryptString($string);
        self::assertTrue((strpos($valueDefault, $prefix) === 0));
    }

    /**
     * @dataProvider providerCrypts
     * @throws Mismatch
     */
    public function testDecrypt($string)
    {
        $prefix = $this->getConfig()->getPrefix();
        $suffix = $this->getConfig()->getSuffix();

        $expected = 'fernando-petry';
        $stringEncrypt = $prefix . $string . $suffix;

        // Entrando uma string criptografada, a saida deve ser o texto descriptografado
        $valueDefault = $this->getCrypt()->decryptString($stringEncrypt);

        self::assertEquals($expected, $valueDefault);

        // Entrando uma string descriptografada, a saída deve ser o próprio texto de entrada
        $valueNotAction = $this->getCrypt()->decryptString($expected);
        self::assertEquals($expected, $valueNotAction);
    }

    public function providerCrypts(): array
    {
        return [
            ['D1dAglwudXp1C5hR0A13fSsrij/jdmsFbVQQsfo1'],
            ['foe2n4VOMP3i2xfRcYoyfliKbsjXn2mKMlXTkZGi'],
            ['fCWOrrtF+2lSzVsYUlyLFNvae9XPxramqwuvvVp4'],
            ['N6u60Prjv5B78VJ3Lweg/G4J29fDEZkc3PZEbBQg'],
            ['aFr+oW89HfgBF4Ia2+TO2PTyCtaiyeyhr80NFpSa'],
        ];
    }

    /**
     * @throws Mismatch
     */
    public function testComplete()
    {
        $expected = 'fernando-petry';

        $crypt = $this->getCrypt()->encryptString($expected);
        $decrypt = $this->getCrypt()->decryptString($crypt);

        self::assertEquals($expected, $decrypt);
    }

    /**
     * @throws Mismatch
     */
    public function testStringTruncated()
    {
        $this->expectException(Exception::class);
        $prefix = $this->getConfig()->getPrefix();

        $expected = 'fernando-petry';
        $stringEncrypt = $prefix . 'D1dAglwudXp1C5hR0A13fSsrij/jdmsFbVQQsfo1';

        // Entrando uma string criptografada, a saida deve ser o texto descriptografado
        $valueDefault = $this->getCrypt()->decryptString($stringEncrypt);

        self::assertEquals($expected, $valueDefault);
    }

}
