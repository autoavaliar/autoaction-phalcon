# Pacote Auto Action para o Framework Phalcon 

Este pacote utiliza seus algorítimos com base no Framework Phalcon para projetos que não utilizam Phalcon.

### Como instalar via composer

```SHELL
composer require autoaction/security
```

### Geração e validação de senha | Usando em um arquivo para gerar senhas criptografadas.
Este pacote visa a validação e geração de hash de senha.

```PHP

<?php
require_once dirname(__FILE__) . '/vendor/autoload.php';
use \AutoAction\Phalcon\Security;

/**
 * Gerando um hash de senha para armazenar no banco de dados
 */
$password = new Security();
$hash = $password->hash('PASSWORD_HERE');


/**
 * Validando uma senha com o hash armazenado no banco de dados
 */
$validade = new Security();
$passwordIsValid = $validade->checkHash('PASSWORD_HERE','HASH_HERE');
```

### Criptografia de strings

Garanta que suas strings vão ser criptografadas seguindo este modelo

```
<?php

use AutoAction\Phalcon\CryptTool;
use AutoAction\Phalcon\CryptTool\Cipher\CipherAes256Cfb;

$config = new CryptConfig('my-key', 'my-prefix');
$crypt = return new CryptTool($config, new CipherAes256Cfb());

// criptografando um texto
$crypt->encryptString('texto a ser criptografado');

// descriptografando um texto
$crypt->encryptString('string-criptografada');
```